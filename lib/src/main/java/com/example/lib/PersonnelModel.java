package com.example.lib;

public class PersonnelModel  extends HumanModel{
    private String PersonnelCode;
    private String startdate3;
    private String Crosssection3;
    private String University3;
    private String flagmode3;




    public PersonnelModel (String PersonnelCode ){
        this.PersonnelCode=PersonnelCode;
    }

    public boolean isPersonnel() {
        return this.flagmode3 .equals("3") ;
    }

    public String getPersonnelCode() {
        return PersonnelCode;
    }

    public void setPersonnelCode(String personnelCode) {
        PersonnelCode = personnelCode;
    }

    public String getStartdate3() {
        return startdate3;
    }

    public void setStartdate3(String startdate3) {
        this.startdate3 = startdate3;
    }

    public String getCrosssection3() {
        return Crosssection3;
    }

    public void setCrosssection3(String crosssection3) {
        Crosssection3 = crosssection3;
    }

    public String getUniversity3() {
        return University3;
    }

    public void setUniversity3(String university3) {
        University3 = university3;
    }

    public String getFlagmode3() {
        return flagmode3;
    }

    public void setFlagmode3(String flagmode3) {
        this.flagmode3 = flagmode3;
    }
}
