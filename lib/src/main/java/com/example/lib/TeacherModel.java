package com.example.lib;

public class TeacherModel extends HumanModel{
    private String TeacherCode;
    private String startdate2;
    private String Crosssection2;
    private String University;
    private String flagmode;



  public  TeacherModel(String TeacherCode){
      this.TeacherCode=TeacherCode;
  }


    public boolean isStuden() {
        return this.flagmode .equals("2") ;
    }

    public String getTeacherCode() {
        return TeacherCode;
    }

    public void setTeacherCode(String teacherCode) {
        TeacherCode = teacherCode;
    }

    public String getStartdate2() {
        return startdate2;
    }

    public void setStartdate2(String startdate2) {
        this.startdate2 = startdate2;
    }

    public String getUniversity() {
        return University;
    }

    public void setUniversity(String university) {
        University = university;
    }

    public String getFlagmode() {
        return flagmode;
    }

    public void setFlagmode(String flagmode) {
        this.flagmode = flagmode;
    }

    public String getCrosssection2() {

        return Crosssection2;
    }

    public void setCrosssection2(String crosssection2) {
        Crosssection2 = crosssection2;
    }
}
