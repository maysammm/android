package com.example.lib;

public class StudentModel extends HumanModel {
    private String studentCode;
    private String startdate1;
    private String Crosssection1;
    private String flagmode1;




    public boolean isStuden() {
        return this.flagmode1 .equals("1") ;
    }

    public StudentModel(String studentCode ){
        this.studentCode=studentCode;
    }
    public String getStartdate() {
        return startdate1;
    }

    public void setStartdate(String startdate) {
        this.startdate1 = startdate;
    }

    public String getFlagmode1() {

        return flagmode1;
    }

    public void setFlagmode1(String flagmode1) {
        this.flagmode1 = flagmode1;
    }

    public String getCrosssection() {

        return Crosssection1;
    }

    public void setCrosssection(String crosssection) {
        Crosssection1 = crosssection;
    }

    public StudentModel() {

    }

    public String getStudentCode() {
        return this.studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }
}
